package com.streetcontxt.coverage_demo;

import org.junit.Test;
import org.junit.experimental.theories.suppliers.TestedOn;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class TreeOperationsTest {

    @Test
    public void traverseInOrderWithSingleNode() {
        Node<Integer> node = ImmutableNode.<Integer>builder()
                .value(1)
                .build();

        List<Integer> result = TreeOperations.traverseInOrder(node);
        assertEquals(result, Arrays.asList(1));
    }

    @Test
    public void traverseInOrderWithSingleLeftChild() {

        Node<Integer> leftChild = ImmutableNode.<Integer>builder()
                .value(-1)
                .build();

        Node<Integer> root = ImmutableNode.<Integer>builder()
                .value(1)
                .left(leftChild)
                .build();

        List<Integer> result = TreeOperations.traverseInOrder(root);
        assertEquals(result, Arrays.asList(1, -1));
    }

    @Test
    public void traverseInOrderWithLeftChildrenOnly() {

        Node<Integer> thirdLeftChild = ImmutableNode.<Integer>builder()
                .value(-10)
                .build();

        Node<Integer> secondLeftChild = ImmutableNode.<Integer>builder()
                .value(-1)
                .left(thirdLeftChild)
                .build();

        Node<Integer> root = ImmutableNode.<Integer>builder()
                .value(1)
                .left(secondLeftChild)
                .build();

        List<Integer> result = TreeOperations.traverseInOrder(root);
        assertEquals(result, Arrays.asList(1, -1, -10));
    }

        /*
    @Test
    public void isLeafIdenfiesALeaf() {
        Node<Integer> leaf = ImmutableNode.<Integer>builder()
                .value(100)
                .build();

        assertTrue(TreeOperations.isLeaf(leaf));
    }*/

    @Test
    public void isLeafIdenfiesNonLeaf() {
        Node<Integer> child = ImmutableNode.<Integer>builder()
                .value(-1)
                .build();

        Node<Integer> parent = ImmutableNode.<Integer>builder()
                .value(1)
                .left(child)
                .build();


        assertFalse(TreeOperations.isLeaf(parent));
    }

}